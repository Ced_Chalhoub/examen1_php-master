<?php
//VL ne pas oublier de démarrer la session pour l'utiliser
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once ("../controllers/creerActivite.php");
include("templates/header.php")
?>
<main class="container-md">
    <!-- Fil d'ariane -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="#">Journal</a>
            <li class="breadcrumb-item"><a href="#">Cours 1</a></li>
            <li class="breadcrumb-item"><a href="semaine.php">Semaine 1</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter une activité</li>
        </ol>
    </nav>

    <?php
        //@TODO : appeller le contrôleur pour ajouter l'activité
        require_once("../controllers/creerActivite.php");
    /**
     * Vérifies si l'activité a été créée.
     * Si oui, on la transfère de POST à SESSION pour s'en servir dans d'autres pages
     * Finalement, on se sert du controlleur pour créer l'activité
     */
        if(isset($_POST["activite"])){
            $_SESSION["activite"] = $_POST["activite"];
            $activite = creerActivite::ajoutNouvelleActivite($_SESSION["activite"]);
            echo '<p class="display-4 mt-4">L\'activité a été ajoutée à votre journal</p>';
        }

    ?>
    <a class="btn btn-primary mt-4" href="ajoutActivite.php">Ajouter une autre activité</a>
</main>
<?php include("templates/footer.php") ?>