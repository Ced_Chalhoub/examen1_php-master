<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

class creerActivite
{
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    /** Fonction qui vérifie si la chaine est valide selon les critères ci-dessus.
     * @param string $chaine -> La chaine à valider
     * @return bool ->Si la chaine est valide ou non. Si oui, on retourne true. Sinon, false.
     */
    private static function estValide(string $chaine): bool
    {
        //VL ne pas oublier / à la fin
        return preg_match('/^\D+$/', $chaine);
    }

    /** Fonction qui vérifie si les informations du formulaire sont valides selon la méthode estValide(). Si oui, on
     * créé l'activité et on l'ajoutes au tableau des activités.
     * @param array $details --> Les infos du formulaire reçu à vérifier
     * @return bool --> Si l'activité a été ajouté, on retourne true. Sinon, false.
     */
    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide
        // ET et non OU : on veut que tous les champs texte soient valides (retirer tableaux et nombres ex. intensité et plaisir)
        if(self::estValide($details["lieu"]) || self::estValide($details["partenaires"]) || self::estValide($details["type"]) || self::estValide($details["determinant"]) ||
                    self::estValide($details["intensite"]) || self::estValide($details["motivation"]) || self::estValide($details["plaisir"])){
            $creation = true;
        }

        //Si valide, créer l'activité
        $activite = new Activite($details);

        //VL Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }

        //@TODO Si valide, l'ajouter au tableau des activités
        array_push($_SESSION["activites"], $activite);
        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}