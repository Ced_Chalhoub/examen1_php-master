<?php

/**
 * Class Activite
 * Une activité a comme propriétés le lieu, la date, le partenaires, le type, le determinant, l'intensite, la duree, les
 * effets, la motivation et le plaisir. Pour construire l'objet, on se sert d'un tableau contenant ceux-ci venant du
 * formulaire sur ajoutActivite.php.
 */
class Activite
{
    private string $lieu;
    private string $date;
    private string $partenaires;
    private string $type;
    private string $determinant;
    private string $intensite;
    private string $duree;
    private array $effets;
    private string $motivation;
    private string $plaisir;

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getPartenaires(): string
    {
        return $this->partenaires;
    }

    /**
     * @param string $partenaires
     */
    public function setPartenaires(string $partenaires): void
    {
        $this->partenaires = $partenaires;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDeterminant(): string
    {
        return $this->determinant;
    }

    /**
     * @param string $determinant
     */
    public function setDeterminant(string $determinant): void
    {
        $this->determinant = $determinant;
    }

    /**
     * @return string
     */
    public function getIntensite(): string
    {
        return $this->intensite;
    }

    /**
     * @param string $intensite
     */
    public function setIntensite(string $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return string
     */
    public function getDuree(): string
    {
        return $this->duree;
    }

    /**
     * @param string $duree
     */
    public function setDuree(string $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return array
     */
    public function getEffets(): array
    {
        return $this->effets;
    }

    /**
     * @param array $effets
     */
    public function setEffets(array $effets): void
    {
        $this->effets = $effets;
    }

    /**
     * @return string
     */
    public function getMotivation(): string
    {
        return $this->motivation;
    }

    /**
     * @param string $motivation
     */
    public function setMotivation(string $motivation): void
    {
        $this->motivation = $motivation;
    }

    /**
     * @return string
     */
    public function getPlaisir(): string
    {
        return $this->plaisir;
    }

    /**
     * @param string $plaisir
     */
    public function setPlaisir(string $plaisir): void
    {
        $this->plaisir = $plaisir;
    }

    /**
     * Activite constructor.
     * @param array $activiteDetails -> Le tableau contient l'information du formulaire. On s'en sert pour créer notre activité.
     */
    public function __construct(array $activiteDetails)
    {
        $this->lieu = isset( $activiteDetails["lieu"]) ? $activiteDetails["lieu"] : "";
        $this->date = isset( $activiteDetails["date"]) ? $activiteDetails["date"] : "";
        $this->type = isset( $activiteDetails["type"]) ? $activiteDetails["type"] : "";
        $this->duree = isset( $activiteDetails["duree"]) ? $activiteDetails["duree"] : "";
        //VL Effets est un tableau
        $this->effets = isset( $activiteDetails["effets"]) ? $activiteDetails["effets"] : [];
        $this->partenaires = isset( $activiteDetails["partenaires"]) ? $activiteDetails["partenaires"] : "";
        $this->determinant = isset( $activiteDetails["determinant"]) ? $activiteDetails["determinant"] : "";
        $this->intensite = isset( $activiteDetails["intensite"]) ? $activiteDetails["intensite"] : "";
        $this->motivation = isset( $activiteDetails["motivation"]) ? $activiteDetails["motivation"] : "";
        $this->plaisir = isset( $activiteDetails["plaisir"]) ? $activiteDetails["plaisir"] : "";
    }


}