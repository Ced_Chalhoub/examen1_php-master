<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    static function setUpBeforeClass(): void
    {
        //VL il manque des données pour pouvoir exécuter les tests
        $_SESSION = array();
    }
    //@TODO implémenter ce test
    public function testEstValideChaineValide():void
    {
        array_push($_SESSION, "Nathalie, Jean-Marc et ‘tite laine");
        $this->assertEquals(true, creerActivite::ajoutNouvelleActivite($_SESSION));
    }

    //@TODO implémenter ce test
    public function testEstValideChaineInValide():void
    {
        array_push($_SESSION, "33 Cité-des-Jeunes ");
        $this->assertEquals(false, creerActivite::ajoutNouvelleActivite($_SESSION));
    }
    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
